from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from datetime import datetime
from django.shortcuts import render



def home(request):
    """ Exemple de page non valide au niveau HTML pour que l'exemple soit concis """
    return HttpResponse("""
        <h1>Bienvenue à Warrior Adventure!</h1>
        <p>I Believe I can Fly ! </p>
    """)

def view_article(request, id_article):
    if id_article > 100:
        raise Http404

    return redirect(view_redirection)

def view_redirection(request):
    return HttpResponse("Vous avez été redirigé.")

def list_articles(request, year, month=1):
    """ Liste des articles d'un mois précis. """
    return HttpResponse('Articles de %s/%s' % (year, month))
    
#àverif
def list_articles_by_tag(request, id_category):
    """ Liste des articles par catégorie. """
    return HttpResponse(
        "Vous avez demandé les catégorie d'article de {0} .".format(id_category)  
    )




def date_actuelle(request):
    return render(request, 'blog/date.html', {'date': datetime.now()})


def addition(request, nombre1, nombre2):    
    total = nombre1 + nombre2

    # Retourne nombre1, nombre2 et la somme des deux au tpl
    return render(request, 'blog/addition.html', locals())

 